// 'use strict'; // eslint-disable-line

// import { main } from 'ts-winlss-agent-script'

const winlss = require('ts-winlss-agent-script')

const config = require('./config')

const mainDir = __dirname

winlss.main(config, mainDir)
