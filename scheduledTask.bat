set CURRENT_FOLDER=%~dp0
set LOG_FILE=%CURRENT_FOLDER%log.txt

echo Script was started... (%date% %time%) > "%LOG_FILE%"

cd %CURRENT_FOLDER%

@rem prevent error durring GIT PULL (if there are local changes)
git reset --hard

@rem execute GIT PULL
git pull

@rem execute YARN
call yarn

@rem execute NODE SCRIPT
node "%CURRENT_FOLDER%index.js" >> "%LOG_FILE%"

echo Script was finished... (%date% %time%) >> "%LOG_FILE%"
